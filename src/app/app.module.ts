import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { AuthService } from "./container/auth/services/auth.service";
import { HttpClientModule } from "@angular/common/http";
import { StorageService } from "./shared/services/storage.service";
import { UserService } from "./container/user/services/user.service";
import { SharedModule } from './shared/shared.module';
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    HttpClientModule,

  ],
  providers: [AuthService, StorageService, UserService],
  bootstrap: [AppComponent],
})
export class AppModule {}
