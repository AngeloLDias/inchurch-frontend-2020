import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class StorageService {
  public storage = window.localStorage;

  constructor() {
  }

  public setItem(key: string, item: any) {
    const value = JSON.stringify(item);
    this.storage.setItem(key, value);
  }
  public getItem(key: string): any {
    const value = JSON.parse(this.storage.getItem(key));
    return value ? value : null;
  }
  public clear() {
    this.storage.clear();
  }
}
