import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { StorageService } from "./storage.service";

@Injectable({
  providedIn: "root",
})
class Observer {
  next: Function;
  error: Function;
  complete: Function;
}

export class SingleResponse {
  public status: number;
  public IsSuccess: boolean;
  public details: string;
}

export class Responses {
  public errors: SingleResponse[] = [];
  public success: SingleResponse[] = [];
}

export class BaseService {
  private header = new HttpHeaders();

  constructor(public http: HttpClient, public storage: StorageService) {
    const userLogged = storage.getItem("userLogged");
    if (userLogged) {
      this.header = this.header.append(
        "Authorization",
        "Bearer " + userLogged.token
      );
    }
  }

  post(url: string, body: object): Observable<Responses[]> {
    let response = this.http.post<Responses>(url, body);
    return Observable.create((observer: Observer) => {
      response.subscribe(
        (res) => {
          observer.next(res);
          observer.complete();
        },
        (error) => {
          observer.error([{ title: error.name, detail: "", error }]);
        }
      );
    });
  }

  put(url: string, body: object): Observable<Responses[]> {
    let response = this.http.put<Responses>(url, body);
    return Observable.create((observer: Observer) => {
      response.subscribe(
        (res) => {
          if (res.errors.length > 0) observer.error(res.errors);
          else observer.next(res.success);
          observer.complete();
        },
        (error) => {
          observer.error([{ title: error.name, detail: "", error }]);
        }
      );
    });
  }

  get<T>(url: string): Observable<T> {
    let response = this.http.get<Responses & T>(url);
    return Observable.create((observer: Observer) => {
      response.subscribe(
        (response) => {
          console.log(response);
          if (response.errors && response.errors.length > 0)
            observer.error(response.errors);
          else {
            observer.next(response);
          }
          observer.complete();
        },
        (error) => {
          observer.error([{ title: error.name, detail: "", error }]);
        }
      );
    });
  }

  delete(url: string) {
    let response = this.http.delete<Responses>(url);
    return Observable.create((observer: Observer) => {
      response.subscribe(
        (response) => {
          observer.next(response);
          observer.complete();
        },
        (error) => {
          observer.error([{ title: error.name, detail: "", error }]);
        }
      );
    });
  }
}
