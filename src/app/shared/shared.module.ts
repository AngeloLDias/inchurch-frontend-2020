import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ButtonComponent } from "./components/UI/button/button.component";
import { CardComponent } from "./components/UI/card/card.component";
import { ImgComponent } from "./components/UI/img/img.component";
import { TitleComponent } from "./components/UI/title/title.component";
import { SubTitleComponent } from "./components/UI/sub-title/sub-title.component";

@NgModule({
  declarations: [
    ButtonComponent,
    CardComponent,
    ImgComponent,
    TitleComponent,
    SubTitleComponent,
  ],
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonComponent,
    CardComponent,
    ImgComponent,
    TitleComponent,
    SubTitleComponent,
  ],
})
export class SharedModule {}
