import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuard } from "./container/auth/guards/auth.guard";
import { AddUserComponent } from './container/user/pages/add-user/add-user.component';
const routes: Routes = [
  {
    path: "",
    redirectTo: "login",
    pathMatch: "full",
  },
  {
    path: "login",
    loadChildren: () =>
      import("./container/auth/auth.module").then((m) => m.AuthModule),
  },
  {
    path: "users",
    loadChildren: () =>
      import("./container/user/user.module").then((m) => m.UserModule),
    canActivate: [AuthGuard],
  },
  { path: "add-user", component: AddUserComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard],
})
export class AppRoutingModule {}
