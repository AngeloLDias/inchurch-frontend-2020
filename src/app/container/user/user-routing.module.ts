import { NgModule, Component } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ListUsersComponent } from "./pages/list-users/list-users.component";
import { AddUserComponent } from "./pages/add-user/add-user.component";
import { UpdateUserComponent } from "./pages/update-user/update-user.component";

const routes: Routes = [
  {
    path: ":page",
    component: ListUsersComponent,
  },
  { path: "update-user/:id", component: UpdateUserComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserRoutingModule {}
