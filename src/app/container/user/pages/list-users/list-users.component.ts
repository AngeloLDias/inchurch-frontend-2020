import { Component, OnInit } from "@angular/core";
import { UserService } from "../../services/user.service";
import { PageUserModel } from "../../models/page-user-model";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslationWidth } from '@angular/common';

@Component({
  selector: "app-list-users",
  templateUrl: "./list-users.component.html",
  styleUrls: ["./list-users.component.scss"],
})
export class ListUsersComponent implements OnInit {
  users: PageUserModel;
  pages = [];
  constructor(
    private servUser: UserService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((res) => {
      this.getUsers(res.page);
    });
  }

  getUsers(page) {
    this.servUser.getUsers(page).subscribe((users) => {
      this.users = users;
      this.getPages();
    });
  }

  getPages() {
    this.pages = [];
    for (let index = 0; index < this.users.total_pages; index++) {
      this.pages.push(index);
    }
  }

  getPage(page) {
    this.router.navigate(["users/" + page]);
  }

  getPageAddUser() {
    this.router.navigate(["add-user"]);
  }

  getPageUpdateUser(user){
    this.router.navigate(["users/update-user/" + user.id])
  }

  deleteUser(user){
    this.servUser.deleteUser(user.id).subscribe(result =>{
      console.log(result)
    })
  }
}
