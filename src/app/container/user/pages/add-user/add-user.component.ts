import { Component, OnInit } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { UserService } from "../../services/user.service";

@Component({
  selector: "app-add-user",
  templateUrl: "./add-user.component.html",
  styleUrls: ["./add-user.component.scss"],
})
export class AddUserComponent implements OnInit {
  formAddUser: FormGroup;

  constructor(private fb: FormBuilder, private servUser: UserService) {}

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.formAddUser = this.fb.group({
      name: ["", Validators.required],
      first_name: ["", Validators.required],
      last_name: ["", Validators.required],
    });
  }

  addUser() {
    this.servUser.addUser(this.formAddUser.value).subscribe((result) => {
      console.log(result);
    });
  }
}
