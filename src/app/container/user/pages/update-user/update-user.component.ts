import { Component, OnInit } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { UserService } from "../../services/user.service";
import { ActivatedRoute } from "@angular/router";
import { UserModel } from "../../models/user-model";

@Component({
  selector: "app-update-user",
  templateUrl: "./update-user.component.html",
  styleUrls: ["./update-user.component.scss"],
})
export class UpdateUserComponent implements OnInit {
  formUpdateUser: FormGroup;
  params: any;
  constructor(
    private fb: FormBuilder,
    private servUser: UserService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.getParams();
  }

  getParams() {
    this.route.params.subscribe((params) => {
      this.getUser(params.id);
      this.params = params;
    });
  }

  initForm(user: UserModel) {
    this.formUpdateUser = this.fb.group({
      avatar: [user.avatar, Validators.required],
      name: [user.email, Validators.required],
      first_name: [user.first_name, Validators.required],
      last_name: [user.last_name, Validators.required],
    });
  }

  getUser(id) {
    this.servUser.getUser(id).subscribe((result: any) => {
      this.initForm(result.data);
    });
  }

  updateUser() {
    console.log(this.formUpdateUser.value)
    this.servUser.updateUser(this.formUpdateUser.value, this.params.id).subscribe((result) => {
      console.log(result);
    });
  }
}
