import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { UserRoutingModule } from "./user-routing.module";
import { SharedModule } from "../../shared/shared.module";
import { ListUsersComponent } from "./pages/list-users/list-users.component";
import { AddUserComponent } from "./pages/add-user/add-user.component";
import { UpdateUserComponent } from "./pages/update-user/update-user.component";

@NgModule({
  declarations: [ListUsersComponent, AddUserComponent, UpdateUserComponent],
  imports: [CommonModule, UserRoutingModule, SharedModule],
})
export class UserModule {}
