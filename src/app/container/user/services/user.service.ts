import { Injectable } from "@angular/core";
import { BaseService } from "src/app/shared/services/base.service";
import { HttpClient } from "@angular/common/http";
import { StorageService } from "src/app/shared/services/storage.service";
import { environment } from "src/environments/environment";
import { PageUserModel } from "../models/page-user-model";

@Injectable({
  providedIn: "root",
})
export class UserService extends BaseService {
  constructor(public http: HttpClient, public storage: StorageService) {
    super(http, storage);
  }

  getUsers(page: number) {
    return this.get<PageUserModel>(`${environment.base_url}users?page=${page}`);
  }

  addUser(obj: object) {
    return this.post(`${environment.base_url}users`, obj);
  }

  updateUser(obj: object, id){
    return this.put(`${environment.base_url}users/${id}`, obj)
  }

  getUser(id: number){
    return this.get(`${environment.base_url}users/${id}`)
  }

  deleteUser(id){
    return this.delete(`${environment.base_url}users/${id}`)
  }
}
