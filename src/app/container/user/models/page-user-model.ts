import { UserModel } from "./user-model"

export class PageUserModel {
    page: number
    per_page: string
    total: 12
    total_pages: 2
    data: UserModel[]
}