import { Injectable } from "@angular/core";
import { CanActivate, Router, RouterStateSnapshot } from "@angular/router";

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {}

  canActivate(): boolean {
    if (localStorage.getItem("token")) {
      return true;
    }
    console.log(this.router.url);
    this.router.navigate(["/login"]);
    return false;
  }
}
