import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "../../services/auth.service";
import { Router } from "@angular/router";
import { StorageService } from "src/app/shared/services/storage.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  formLogin: FormGroup;
  constructor(
    private fb: FormBuilder,
    private servAuth: AuthService,
    private router: Router,
    private storage: StorageService
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.formLogin = this.fb.group({
      email: ["eve.holt@reqres.in", Validators.required],
      password: ["cityslicka", Validators.required],
    });
  }

  login() {
    this.servAuth.login(this.formLogin.value).subscribe((result: any) => {
      this.storage.setItem("token", result.token);
      this.router.navigate([`users/${1}`]);
      console.log(result);
    });
  }
}
