import { Injectable } from "@angular/core";
import { BaseService } from "src/app/shared/services/base.service";
import { HttpClient } from "@angular/common/http";
import { environment } from 'src/environments/environment';
import { StorageService } from 'src/app/shared/services/storage.service';

@Injectable({
  providedIn: "root",
})
export class AuthService extends BaseService {
  constructor(public http: HttpClient, public storage: StorageService) {
    super(http, storage);
  }

  login(obj){
    return this.post(`${environment.base_url}login`, obj)
  }

}
