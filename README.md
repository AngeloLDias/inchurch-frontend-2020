# inChurch Developer Recruitment 2020 #

This stage of the recruitment process consists on the development of a small CRUD application in Angular 7+ with the following features:

* List of registered users
* User detail
* User update
* User creation
* User deletion

This should be done by consuming the REST API provided by [Reqres](https://reqres.in/).

### What should I do? ###

* Fork this repository (Click on the '+' button on the left menu and then on 'Fork this repository')
* Access [Reqres](https://reqres.in/) and learn about their API. You should create a CRUD aplication based on the requests available
* Develop the aplication using best practices (write your code and comments in english)
* Create a pull request when you are done

### What is going to be evaluated? ###

* Code quality
* Application appearance
* Git usage
* Development time
* Surprising us

### Do you have any questions? ###

Contact: rafael.reis@inchurch.com.br